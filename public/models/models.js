var config = {
    markerSize: 30,
    markerFirstColor: 0xff0000,
    markerSecondColor: 0xffff00
}
var view =
{
    camera: {
        fov: 40,
        near: 1,
        far: 50000,
        x: 0,
        y: 500,
        z: 4000
    },
    ambientLight: {
        color: 0xffffff,
        intensity: 0,
        connectWithCamera: false,
        x: 1000,
        y: 2000,
        z: 1500
    },
    spotLight: {
        color: 0xffffff,
        intensity: 1,
        x: 1000,
        y: 1500,
        z: 1500,
        distance: 7000,
        angle: Math.PI / 4,
        castShadow: true
    },
    sky: {
        radius: 10000,
        resolution: 320
    },
    floor: {
        size: 20000,
        color: 0xffffaa
    },
    tv: {
        width: 2000,
        x: -1150,
        y: 500,
        z: 3000
    },
    wall: {
        size: 3000,
        divisions: 6,
        firstColor: 0x888888,
        secondColor: 0x888888
    },
    objects:
        [
            {
                id: "carpet",
                type: "box",
                width: 3000,
                height: 2,
                depth: 3000,
                faceColor: 0x333333,
                edgeColor: 0x006600,
                showEdges: false,
                x: -500,
                y: -1000,
                z: 0
            }
        ]
};

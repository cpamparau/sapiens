﻿var UtilUSV = new function () {

    var sp = []; // position of sphere
    var s = []; // sphere
    var a, b;
    var b1p, b2p, b1g, b2g;
    var refresh;
    var STANDARD_REFRESH_TIME = 100;

    this.setup = function (camera, renderer, scene) {
        controls = new THREE.OrbitControls(camera, renderer.domElement);
        controls.target.set(0, 0, 0);

        camera.lookAt(new THREE.Vector3(0, 0, 0));

        // enable vibration support
        navigator.vibrate = navigator.vibrate || navigator.webkitVibrate || navigator.mozVibrate || navigator.msVibrate;

        // for chrome on andriod:
        // chrome://flags
        // Disable gesture requirement for media playback

        a = document.createElement("AUDIO");
        a.setAttribute("src", "a.mp3");
        a.pause();
        b = document.createElement("AUDIO");
        b.setAttribute("src", "b.mp3");
        b.pause();

        for (var i = 0; i < 10; i++) {
            var geometry = new THREE.SphereGeometry(100, 32, 32);
            var material = new THREE.MeshBasicMaterial({ color: 0xffff00 });
            s[i] = new THREE.Mesh(geometry, material);
            sp[i] = s[i].position;
            scene.add(s[i]);
        }
        b1p = scene.getObjectByName("Box1").position;
        b2p = scene.getObjectByName("Box2").position;

        b1g = scene.getObjectByName("Box1").geometry.parameters;
        b2g = scene.getObjectByName("Box2").geometry.parameters;

        refresh = setInterval(ajx, STANDARD_REFRESH_TIME);
    };
    this.loop = function () {
        (isInside(sp[0], b1p, b1g) || isInside(sp[1], b1p, b1g)) ? a.play() : a.pause();
        (isInside(sp[0], b2p, b2g) || isInside(sp[1], b2p, b2g)) ? b.play() : b.pause();
        if (navigator.vibrate)
            (isInside(sp[0], b1p, b1g) || isInside(sp[1], b1p, b1g) || isInside(sp[0], b2p, b2g) || isInside(sp[1], b2p, b2g)) ? navigator.vibrate(1000) : navigator.vibrate(0);
    }
    function ajx() {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                // get data
                if (typeof (xhttp.responseText) !== 'undefined') {
                    console.log(xhttp.responseText);
                    if (xhttp.responseText.length > 10) {
                        var data = JSON.parse(xhttp.responseText);
                        for (var i = 0; i < 10; i++) {
                            sp[i].x = sp[i].y = sp[i].z = 0;
                        }
                        for (var i = 0; i < data.airObjects.length; i++) {
                            sp[i].x = data.airObjects[i].y;
                            sp[i].y = data.airObjects[i].z;
                            sp[i].z = data.airObjects[i].x;
                            s[i].material.color.setHex(parseInt("0x"+data.airObjects[i].color));
                        }

                    }
                    clearInterval(refresh);
                    if (typeof (data) !== 'undefined') {
                        refresh = setInterval(ajx, data.refresh);
                    }
                    else {
                        refresh = setInterval(ajx, STANDARD_REFRESH_TIME);
                    }
                }
            }
        };
        // use the date to permanently change query string to prevent caching
        var d = new Date();
        xhttp.open("GET", "data.txt?d="+d.getTime(), true);
        xhttp.send();
    }
    function isInside(sp, bp, bg) {
        return (sp.x > bp.x - bg.width / 2 && sp.x < bp.x + bg.width / 2 && sp.y > bp.y - bg.height / 2 && sp.y < bp.y + bg.height / 2 && sp.z > bp.z - bg.depth / 2 && sp.z < bp.z + bg.depth / 2);
    }
}
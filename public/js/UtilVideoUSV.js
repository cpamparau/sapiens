﻿var UtilUSV = new function () {

    var sp = []; // position of sphere
    var s = []; // sphere
    var a, b;
    var b1p, b2p, b1g, b2g;
    var refresh;
    var STANDARD_REFRESH_TIME = 10;

    this.setup = function (camera, renderer, scene) {
        // enable vibration support
        navigator.vibrate = navigator.vibrate || navigator.webkitVibrate || navigator.mozVibrate || navigator.msVibrate;

        // for chrome on andriod:
        // chrome://flags
        // Disable gesture requirement for media playback

        a = document.createElement("VIDEO");
        a.setAttribute("src", "a.mp4");
        a.setAttribute("id", "a");
        a.setAttribute("width", "320");
        a.setAttribute("height", "240");
        a.setAttribute("controls", "controls");
        a.setAttribute("preload", "auto");
        document.body.appendChild(a);
        a.pause();

        b = document.createElement("AUDIO");
        b.setAttribute("src", "b.mp3");
        b.pause();

        for (var i = 0; i < 12; i++) {
            var geometry = new THREE.SphereGeometry(100, 32, 32);
            var material = new THREE.MeshBasicMaterial({ color: 0xffff00 });
            s[i] = new THREE.Mesh(geometry, material);
            sp[i] = s[i].position;
            scene.add(s[i]);
        }
        b1p = scene.getObjectByName("Box1").position;
        b2p = scene.getObjectByName("Box2").position;

        b1g = scene.getObjectByName("Box1").geometry.parameters;
        b2g = scene.getObjectByName("Box2").geometry.parameters;

        refresh = setInterval(ajx, STANDARD_REFRESH_TIME);
    };
    this.loop = function () {
        (isInside(sp[0], b1p, b1g) || isInside(sp[1], b1p, b1g)) ? startPlay(a) : stopPlay(a);
        (isInside(sp[0], b2p, b2g) || isInside(sp[1], b2p, b2g)) ? startPlay(b) : stopPlay(b);
        if (navigator.vibrate)
            (isInside(sp[0], b1p, b1g) || isInside(sp[1], b1p, b1g) || isInside(sp[0], b2p, b2g) || isInside(sp[1], b2p, b2g)) ? navigator.vibrate(1000) : navigator.vibrate(0);
    }
    function startPlay(c) {
        c.setAttribute("style", "visibility: visible;");
        c.play();
    }
    function stopPlay(c) {
        c.setAttribute("style", "visibility: hidden;");
        c.pause();
    }
    var d = new Date();
    var start_time;
    var stop_time;
    function ajx() {
        $.ajax({
            url: "data.txt?d=" + d.getTime(),
            method: 'GET',
            beforeSend: function (request, settings) {
                start_time = performance.now();
            },
            success: function (response) {
                var data = JSON.parse(response);
                for (var i = 0; i < 12; i++) {
                    sp[i].x = sp[i].y = sp[i].z = 0;
                }
                for (var i = 0; i < data.airObjects.length; i++) {
                    sp[i].x = data.airObjects[i].y;
                    sp[i].y = data.airObjects[i].z;
                    sp[i].z = data.airObjects[i].x;
                    s[i].material.color.setHex(parseInt("0x" + data.airObjects[i].color));
                }
                stop_time = performance.now();
                //document.write("" + (stop_time - start_time) + "<br>");//console.log("" + stop_time - start_time + "\n");
            },
            error: function (jqXHR) {
                console.log('ERROR! \n %s', JSON.stringify(jqXHR));
            }
        });
    }
ro
    function isInside(sp, bp, bg) {
        return (sp.x > bp.x - bg.width / 2 && sp.x < bp.x + bg.width / 2 && sp.y > bp.y - bg.height / 2 && sp.y < bp.y + bg.height / 2 && sp.z > bp.z - bg.depth / 2 && sp.z < bp.z + bg.depth / 2);
    }
}

const AVG_INTERVAL = 500;
var isActive = true, isPopulated = false, myVar = 0, myVar1 = 0, myVar2 = 0, count = 0;
let container, scene, camera, controls, renderer, cssRenderer;
let containerID = "view", player, objects = [], ambientLight;
let phone, tablet, sound1, sound2, boxe, tv, boxVR, cameras, vuzix;
let simulationParameters, phoneParameters, showParameters;
var MatthewText, MichaelText, SandraText, SharedText, arrayXR = [], sandra1, sandra2;
let producers = {}, producersParameters = {}, consumer;
console.log(Vicon);
const producersClasses = {
	"Vicon": Vicon,
	"Tablet": Tablet,
	"Microphone": Microphone,
	"Smartphone": Smartphone,
	"EEGHeadset": EEGHeadset,
	"EyeTracker": EyeTracker,
	"Vuzix": Vuzix,
	"HTCVive": HTCVive,
	"HoloLens": HoloLens 
};
let messages = [], activeMessageType = "Vicon";
let attentionDetectionModule, contextAwarenessModule, priorityManagementModule, interruptibilityPredictionModule;
const EUPHORIA_INPUT_ADDRESS = "wss://euphoria2--oschipor.repl.co/input";
const EUPHORIA_OUTPUT_ADDRESS = "wss://euphoria2--oschipor.repl.co/output";

function dom(id) {
	return document.getElementById(id);
}
function getRndInteger(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}
function getRndFloat(min, max) {
	return Math.random() * (max - min) + min;
};
// functie apelata la incarcarea obiectelor COLLADA
function setModel(collada, name, scaleX, scaleY, scaleZ, positionX, positionY, positionZ, rotationX = 2*Math.PI, rotationY = 2*Math.PI, rotationZ=2*Math.PI) {
	collada.scene.scale.set(scaleX, scaleY, scaleZ);
	collada.scene.position.x = positionX;
	collada.scene.position.y = positionY;
	collada.scene.position.z = positionZ;
	collada.scene.name = name;
	if (rotationY !== 2*Math.PI) collada.scene.rotation.y = rotationY;
	if (rotationX !== 2*Math.PI) collada.scene.rotation.x = rotationX;
	if (rotationZ !== 2*Math.PI) collada.scene.rotation.z = rotationZ;
	scene.add(collada.scene);
	
}
// functie de adaugare a teleonului, tabletei, sound si TV
function addPrimitivesInScene(object, typeGeometry,XGeometry, YGeometry, ZGeometry, castShadow, receiveShadow, PosX, PosY, PosZ, 
	RotationX, RotationY, RotationZ, _name = "", materialIsVisible = true, _color=0x222222){
	var geometry;
	if (typeGeometry === "Box")
		geometry = new THREE.BoxGeometry(XGeometry, YGeometry, ZGeometry);
   else
	   geometry = new THREE.SphereGeometry(XGeometry, YGeometry, ZGeometry);
   var material = new THREE.MeshStandardMaterial({color: _color, metalness: 0, roughness: 0, shading: THREE.SmoothShading});
   object = new THREE.Mesh(geometry, material);
   object.castShadow = castShadow;
   object.receiveShadow = receiveShadow;
   object.position.x = PosX;
   object.position.y = PosY;
   object.position.z = PosZ;
   object.name = _name;
   if (RotationX !== 2*Math.PI) object.rotation.x = RotationX;
   if (RotationY !== 2*Math.PI) object.rotation.y = RotationY;
   if (RotationZ !== 2*Math.PI) object.rotation.z = RotationZ;
   object.material.visible = materialIsVisible;
   scene.add(object);
   return object;
}

function loadModels(){
	//new THREE.ColladaLoader().load("models/pictures.dae", (collada) => { setModel(collada, "Pictures",14, 14, 15, 500, -1000, 1200, -Math.PI/2, 2*Math.PI, Math.PI/2) });
	new THREE.ColladaLoader().load('models/screen.dae', (collada) => { setModel(collada, "Screen", 14, 14, 14, 500, -1000, 1200, -Math.PI/2, 2*Math.PI, Math.PI/2) });
	new THREE.ColladaLoader().load('models/table.dae', (collada) => { setModel(collada, "Table", 14, 14, 14, 500, -1000, 1200, -Math.PI/2, 2*Math.PI, Math.PI/2) });
	new THREE.ColladaLoader().load('models/sound.dae', (collada) => { setModel(collada, "Sound", 14, 14, 14, 500, -1000, 1200, -Math.PI/2, 2*Math.PI, Math.PI/2) });
	new THREE.ColladaLoader().load('models/woman.dae', (collada) => { setModel(collada, "Woman", 14, 14, 14, 500, -1000, 1200, -Math.PI/2, 2*Math.PI, Math.PI/2) });
	new THREE.ColladaLoader().load('models/Matthew.dae', (collada) => { setModel(collada, "Matthew", 5, 5, 5, -200, -1000, 800, -Math.PI/2, 2*Math.PI, Math.PI/2) });
	new THREE.ColladaLoader().load('models/Michael.dae', (collada) => { setModel(collada, "Michael", 13, 13, 13, 350, -1000, 800, -Math.PI/2, 2*Math.PI, -Math.PI/1.9) });
	//new THREE.ColladaLoader().load('models/cameras.dae', (collada) => {setModel(collada, "Cameras", 14, 14, 14, 500, -1000, 1200, -Math.PI/2, Math.PI*2, Math.PI/2);});
	new THREE.ColladaLoader().load('models/AR.dae', (collada) => { setModel(collada, "Vuzix", 0.04, 0.04, 0.04, -914,-160,940,Math.PI*1.5,Math.PI*2,Math.PI/1.9) });
	new THREE.ColladaLoader().load('models/htcheadset.dae', (collada) => { setModel(collada, "HTCVive", 21, 21, 21, -250, -160, -220, Math.PI/2, Math.PI, Math.PI*0.95) }); // 2*Math.PI,Math.PI/2,2*Math.PI
	new THREE.ColladaLoader().load('models/htccontroller.dae', (collada) => { setModel(collada, "HTCController1", 15, 15, 15, -50, -665, -290, Math.PI/2.5, Math.PI, 2*Math.PI) });
	new THREE.ColladaLoader().load('models/htccontroller.dae', (collada) => { setModel(collada, "HTCController2", 15, 15, 15, -375, -665, -100, Math.PI/2.5, Math.PI, 2*Math.PI) });
	new THREE.ColladaLoader().load('models/MSHololens.dae', (collada) => { setModel(collada,"HoloLens", 13, 13, 13, 250, -130, 770, Math.PI/2, Math.PI, Math.PI/1.9) });
}
function createScene() {
	scene = new THREE.Scene();
	addContainerInScene(containerID);
	addCameraInScene();
	addControlsInScene();
	addLightsInScene();
	addSkyDomeInScene();
	//addWallsInScene();
	//addFloorInScene();
	addDefaultObjectsInScene();
	phone = addPrimitivesInScene(phone,"Box",120,75,10, true, true, 400,-700,-600,-Math.PI/2, 2*Math.PI, 2*Math.PI);
	tablet = addPrimitivesInScene(tablet, "Box", 150,200,20,true, true, -350,-350,-80,2*Math.PI,-Math.PI/8,2*Math.PI);
	sound1 = addPrimitivesInScene(sound1,"Box",65,130,3, false,  true, -1595, 56, 1310, 2*Math.PI, Math.PI/1.23, 2*Math.PI);
	sound2 = addPrimitivesInScene(sound2,"Box",65,130,3,  false,  true, 470, 56, 1310, 1*Math.PI, Math.PI/1.23, 2*Math.PI);
	tv = addPrimitivesInScene(tv, "Box", 600,400,20, false, true, -1800,-350,-400,2*Math.PI, Math.PI/2, 2*Math.PI, "",false);
	//boxVR = addPrimitivesInScene(boxVR, "Box", 200, 200,200, false, true, -1400, -150, -100, 2*Math.PI, 2*Math.PI, 2*Math.PI, false);
	// MatthewText = addCharacterText("Matthew","Mixed Reality", 90, -150, 850, Math.PI/3);
	MatthewText = addPrimitivesInScene(MatthewText, "Box",300,100,10,false, true,90,-150,850,2*Math.PI, Math.PI/3,2*Math.PI, "Mixed Reality",true, '#D60808');
	// MichaelText = addCharacterText("Mark", "Augmented Reality",-810, -200, 850, Math.PI*1.69);
	MichaelText = addPrimitivesInScene(MichaelText, "Box", 300,100,10, false, true, -810,-200,850, 2*Math.PI, Math.PI*1.69, 2*Math.PI, "Augmented Reality", true,'#D60808');
	// SandraText = addCharacterText("Sandra", "Virtual Reality",-200, -100, 50, Math.PI);
	SandraText = addPrimitivesInScene(SandraText, "Box", 300,100,10, false, true, -250,-200,150, 2*Math.PI, Math.PI, 2*Math.PI, "Virtual Reality", true, '#D60808');
	sandra1 = addPrimitivesInScene(sandra1, "Box", 300,100,10, false, true, -500, -200, 50, 2*Math.PI, Math.PI*1.69, 2*Math.PI, "Sandra1", true, '#D60808');
	sandra2 = addPrimitivesInScene(sandra2, "Box", 300,100,10, false, true, -10,-200,50, 2*Math.PI, Math.PI/3, 2*Math.PI, "Sandra2", true, '#D60808');
	// SharedText = addCharacterText("Public", "Shared Object",-200, -300, 550, Math.PI);
	SharedText = addPrimitivesInScene(SharedText, "Box", 400, 200, 20,false, true, -300, -300, 550, 2*Math.PI, Math.PI, 2*Math.PI, "Shared Object", true,'#D60808');
	loadModels();  	//------------   COLLADA OBJECTS  -------------------------// 
	animateObjectsInScene();
	startSimulation();
	addControlPanelInScene();
	initializeRenderer();

	//onMouse();
}

function addCharacterText(textName, meshName, posX, posY, posZ, rotationY){
	var loader = new THREE.FontLoader();
	var mesh;
	loader.load( './js/helvetiker_bold.js', function ( font ) {
		var textGeo = new THREE.TextGeometry(textName, {
			font: font,
			size: 50, // font size
			height: 40, // how much extrusion (how thick / deep are the letters)
			curveSegments: 12,
			bevelThickness: 1,
			bevelSize: 1,
			bevelEnabled: true
		});
		textGeo.computeBoundingBox();
		var textMaterial = new THREE.MeshPhongMaterial( { color: 0xff0000, specular: 0xffffff } );
		mesh = new THREE.Mesh( textGeo, textMaterial );
		mesh.position.x = posX ;// -750
		mesh.position.y = posY; // -100
		mesh.position.z = posZ; // 1100
		mesh.rotation.y = rotationY; // Math.PI*1.69
		mesh.castShadow = false;
		mesh.receiveShadow = true;
		mesh.name = meshName;
		scene.add( mesh );
		//console.log(mesh);
		//mesh.on('Click', onXRObjects("Augmented Reality"))
	});
	return mesh;
}
function getDeltaScale(element, smallDeltaScale, bigDeltaScale) {
	let scale = 0.9;
	if (element.scale.x > 0 && element.scale.x < 0.9) {
		scale = bigDeltaScale;
	}
	if (element.scale.x > 0.9) {
		scale = smallDeltaScale;
	}
	return scale;
}
function createPanel(panel, panelTitle, panelContent) {
	panelTitle.click(function () {
		panelContent.toggle();
	});
	panel.on('mousedown wheel', function (e) {
		e.stopPropagation();
	});
}
function addContainerInScene() {
	container = document.getElementById(containerID);
	container.parentElement.removeChild(container);
	container = document.createElement("div");
	container.id = containerID;
	document.body.appendChild(container);
}
function addCameraInScene() {
	camera = new THREE.PerspectiveCamera(view.camera.fov, window.innerWidth / window.innerHeight, view.camera.near, view.camera.far);
	camera.position.set(view.camera.x, view.camera.y, view.camera.z);
	scene.add(camera);
}
function addControlsInScene() {
	controls = new THREE.OrbitControls(camera);
	controls.maxPolarAngle = 0.9 * Math.PI / 2;
}
function addLightsInScene() {
	// ambient light
	ambientLight = new THREE.DirectionalLight(view.ambientLight.color, view.ambientLight.intensity);
	ambientLight.position.x = view.ambientLight.x;
	ambientLight.position.y = view.ambientLight.y;
	ambientLight.position.z = view.ambientLight.z;
	if (view.ambientLight.connectWithCamera) {
		camera.add(ambientLight);
	}
	else {
		scene.add(ambientLight);
	}
	// spot light
	var spotLight = new THREE.SpotLight(view.spotLight.color);
	spotLight.position.x = view.spotLight.x;
	spotLight.position.y = view.spotLight.y;
	spotLight.position.z = view.spotLight.z;
	spotLight.distance = view.spotLight.distance;
	spotLight.angle = view.spotLight.angle;
	spotLight.intensity = view.spotLight.intensity;
	spotLight.castShadow = view.spotLight.castShadow;
	spotLight.shadowCameraNear = true;
	scene.add(spotLight);
}
function addSkyDomeInScene() {
	var vertexShader = "varying vec3 vWorldPosition;void main() {vec4 worldPosition = modelMatrix * vec4(position, 1.0);vWorldPosition = worldPosition.xyz;gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);}";//document.getElementById("vertexShader").textContent;
	var fragmentShader = "uniform vec3 topColor;uniform vec3 bottomColor;uniform float offset;uniform float exponent;varying vec3 vWorldPosition;void main() {float h = normalize(vWorldPosition + offset).y;gl_FragColor = vec4(mix(bottomColor, topColor, max(pow(max(h, 0.0), exponent), 0.0)), 1.0);}";//document.getElementById("fragmentShader").textContent;
	var uniforms = {
		topColor: { type: "c", value: new THREE.Color(0x0077ff) },
		bottomColor: { type: "c", value: new THREE.Color(0xffffff) },
		offset: { type: "f", value: 400 },
		exponent: { type: "f", value: 0.6 }
	};
	uniforms.topColor.value.copy(ambientLight.color);

	var skyGeo = new THREE.SphereGeometry(view.sky.radius, view.sky.resolution, view.sky.resolution);
	var skyMat = new THREE.ShaderMaterial({
		uniforms: uniforms,
		vertexShader: vertexShader,
		fragmentShader: fragmentShader,
		side: THREE.BackSide
	});

	var sky = new THREE.Mesh(skyGeo, skyMat);
	scene.add(sky);
}
function addWallsInScene() {
	var bottom = new THREE.GridHelper(view.wall.size / 2, view.wall.divisions, new THREE.Color(0xffffff), new THREE.Color(0xffffff));
	bottom.position.set(-500, -1000, 0);
	scene.add(bottom);

	var rightWall = new THREE.GridHelper(view.wall.size / 2, view.wall.divisions, new THREE.Color(0xffffff), new THREE.Color(0xffffff));
	rightWall.position.set(-500, view.wall.size / 2 - 1000, -view.wall.size / 2);
	rightWall.rotation.x = Math.PI / 2;
	scene.add(rightWall);

	var backWall = new THREE.GridHelper(view.wall.size / 2, view.wall.divisions, new THREE.Color(0xffffff), new THREE.Color(0xffffff));
	backWall.position.set(-view.wall.size / 2 - 500, view.wall.size / 2 - 1000, 0);
	backWall.rotation.z = Math.PI / 2;
	scene.add(backWall);
}
function addFloorInScene() {
	var g = new THREE.PlaneGeometry(view.floor.size, view.floor.size);
	var material = new THREE.MeshStandardMaterial({ color: new THREE.Color(0xffffff), metalness: 0, roughness: 0, side: THREE.DoubleSide });
	var ground = new THREE.Mesh(g, material);
	ground.position.set(0, -1002, 0);
	ground.rotation.x = -Math.PI / 2;
	ground.receiveShadow = false;
	scene.add(ground);
}
function addDefaultObjectsInScene() {
	for (var i = 0; i < view.objects.length; i++) {
		var object = view.objects[i];
		var geometry;
		switch (object.type) {
			case "box": geometry = new THREE.BoxGeometry(object.width, object.height, object.depth);
				break;
			case "sphere": geometry = new THREE.SphereGeometry(object.radius, object.widthSegments, object.heightSegments);
				break;
		}
		objects[i] = addObject(geometry, object.faceColor, object.edgeColor, object.x, object.y, object.z, object.showEdges);
	}
}
function addObject(geometry, faceColor, edgeColor, x, y, z, showEdges) {
	standardMaterial = new THREE.MeshStandardMaterial({ color: faceColor, transparent: true, opacity: 0.4 });
	var cube = new THREE.Mesh(geometry, standardMaterial);
	cube.position.set(x, y, z);
	cube.castShadow = true;
	cube.receiveShadow = true;
	cube.customObject = true;
	scene.add(cube);
	if (showEdges) {
		var geo = new THREE.EdgesGeometry(geometry);
		var mat = new THREE.LineBasicMaterial({ color: edgeColor, linewidth: 1 });
		var wireframe = new THREE.LineSegments(geo, mat);
		wireframe.position.set(x, y, z);
		scene.add(wireframe);
	}
	return cube;
}
function animateObjectsInScene() {
	setInterval(function () {
		if (!phoneParameters["noMessage"]) {
			phone.scale.x = phone.scale.y = phone.scale.z *= getDeltaScale(phone, 0.9, 1.1);
			tv.scale.x = tv.scale.y = tv.scale.z *= getDeltaScale(tv, 0.95, 1.05);
		}
		if (ambientParameters.Surround) {
			sound1.scale.x = sound1.scale.y = sound1.scale.z = sound2.scale.x = sound2.scale.y = sound2.scale.z *= getDeltaScale(sound1, 0.9, 1.1);
		}

		if (producersParameters["Tablet"].onOff && phoneParameters.highPriorityMessage) {
			tablet.scale.x = tablet.scale.y = tablet.scale.z *= getDeltaScale(tv, 0.95, 1.05);
		}
	}, 100);
}
function initializeRenderer() {
	// RENDERER
	renderer = new THREE.WebGLRenderer({ antialias: true });
	renderer.setPixelRatio(window.devicePixelRatio);
	renderer.setSize(window.innerWidth, window.innerHeight);
	container.appendChild(renderer.domElement);
	renderer.gammaInput = true;
	renderer.gammaOutput = true;
	renderer.sortObjects = false;
	renderer.shadowMapEnabled = true;
	renderer.setClearColor(0x000000, 1.0);
	window.addEventListener("resize", onWindowResize, false);
	// CSS RENDERER
	cssRenderer = createCssRenderer();
	document.body.appendChild(cssRenderer.domElement);
}

function addControlPanelInScene() {
	var gui = new dat.GUI();
	document.addEventListener('mousedown', onDocumentMouseDown, false);
	var guiTitle = document.createElement("div");
	guiTitle.innerHTML = "Control Panel";
	guiTitle.id = "guiTitle";
	gui.domElement.insertBefore(guiTitle, gui.domElement.childNodes[0]);
	$(gui.domElement.childNodes[3]).remove();//html("Scroll to zoom in/out, drag & drop to rotate, and right drag & drop to pan.");//remove();

	createPanel($(gui.domElement), $(guiTitle), $(gui.domElement.childNodes[2]));
	$(gui.domElement).css({ "position": "absolute", "left": "", "top": "" });
	gui.domElement.id = "controlPanel";
	dom("controlPanel").style.width = "400px";

	showParameters = {
		json: true,
		js: true
	};
	phoneParameters = {
		//Notification: false,
		noMessage: false,
		lowPriorityMessage: false,
		mediumPriorityMessage: false,
		highPriorityMessage: false
	};
	personalParameters = {
		Vuzix: true,
		HoloLens: true,
		HTCVive: true,
		Tablet: true,
		Smartphone: false,
		EEGHeadset:true,
		EyeTracker:true,
		AttentionModule:true,
		ContextAwareness:true,
		PriorityManagement:true
	};
	ambientParameters = {
		WallDisplay : true,
		Vicon: true,
		Surround: false,
		Microphone: true
	};
	modulesParameters = {
		AttentionModule : true,
		ContextAwareness: true,
		PriorityManagement: true
	};
	XRObjectsParameters ={
		MixedReality: true,
		AugmentedReality: true,
		VirtualReality: true,
		SharedObject: true
	};

	var showFolder = gui.addFolder('Show/Hide panels');
	showFolder.add(showParameters, 'json').name('Show JSON messages').listen().onChange(() => onShowEvent());
	showFolder.add(showParameters, 'js').name('Show JavaScript code').listen().onChange(() => onShowEvent());
	showFolder.open();
	onShowEvent();
	function onShowEvent() {
		(showParameters.js) ? $("#jsPanelContent").show() : $("#jsPanelContent").hide();
		(showParameters.json) ? $("#jsonPanelContent").show() : $("#jsonPanelContent").hide();
	}

	var phoneFolder = gui.addFolder('Notification');
	//var x = phoneFolder.add(phoneParameters, 'Notification', ['No notification', 'Low priority notification', 'Medium priority notification', 'High priority notification']).name('Notification').listen().onChange(() => onPhoneMessage(""));
	phoneFolder.add(phoneParameters, 'noMessage').name('No notification').listen().onChange(() => onPhoneMessage("noMessage"));
	phoneFolder.add(phoneParameters, 'lowPriorityMessage').name('Low priority notification').listen().onChange(() => onPhoneMessage("lowPriorityMessage"));
	phoneFolder.add(phoneParameters, 'mediumPriorityMessage').name('Medium priority notification').listen().onChange(() => onPhoneMessage("mediumPriorityMessage"));
	phoneFolder.add(phoneParameters, 'highPriorityMessage').name('High priority notification').listen().onChange(() => onPhoneMessage("highPriorityMessage"));
	phoneFolder.open();
	onPhoneMessage("noMessage");
	function onPhoneMessage(control) {
		for (let param in phoneParameters) {
			phoneParameters[param] = false;
		}
		phoneParameters[control] = true;
		switch (control) {
			case "noMessage":
				phone.material.color.setHex("0x222222");
				tv.material.visible = false;
				//boxVR.material.visible = false;
				tablet.material.color.setHex("0x222222");
				producers["Smartphone"].setNotificationPriority("");
				PriorityManagement.PriorityLevels["Smartphone"] = "none";
				break;
			case "lowPriorityMessage":
				phone.material.color.setHex("0x00ff00");
				tv.material.visible = true;
				tv.material.color.setHex("0x00ff00");
				tv.position.z = -800;
				// boxVR.position.x = -1400;
				// boxVR.material.visible = true;
				// boxVR.material.transparent = true;
				tablet.material.color.setHex("0x222222");
				producers["Smartphone"].setNotificationPriority("low");
				PriorityManagement.PriorityLevels["Smartphone"] = "low";
				break;
			case "mediumPriorityMessage":
				phone.material.color.setHex("0xffff00");
				tv.material.visible = true;
				tv.material.color.setHex("0xffff00");
				tv.position.z = 0;
				// boxVR.material.visible = true;
				// boxVR.material.transparent = true;
				// boxVR.position.x = -1000;
				tablet.material.color.setHex("0x222222");
				producers["Smartphone"].setNotificationPriority("medium");
				PriorityManagement.PriorityLevels["Smartphone"] = "medium";
				break;
			case "highPriorityMessage":
				phone.material.color.setHex("0xff0000");
				tv.material.visible = true;
				tv.material.color.setHex("0xff0000");
				tv.position.z = 800;
				// boxVR.material.visible = true;
				// boxVR.material.transparent = true;
				// boxVR.position.x = -600;
				tablet.material.color.setHex("0xff0000");
				producers["Smartphone"].setNotificationPriority("high");
				PriorityManagement.PriorityLevels["Smartphone"] = "high";
				break;
		}

		if (control !== "noMessage") {
			producers["Smartphone"].producer.onMessageWrite();
			producers["Smartphone"].onData();
			producers["Smartphone"].producer.sendMessage();
		}
		else {
			messages["Smartphone"] = { data: null };
			producers["Smartphone"].pause();
		}
		onSoundStateChange();
	}

	var softwareModulesFolder = gui.addFolder("Software Modules");
	softwareModulesFolder.add(modulesParameters, "AttentionModule").name('Attention Module').listen().onChange(() => {ambientParameters.AttentionModule = !ambientParameters.AttentionModule});
	softwareModulesFolder.add(modulesParameters, "ContextAwareness").name('Context Awareness').listen().onChange(() => {ambientParameters.ContextAwareness = !ambientParameters.ContextAwareness});
	softwareModulesFolder.add(modulesParameters, "PriorityManagement").name('Priority Management').listen().onChange(() => {ambientParameters.PriorityManagement = !ambientParameters.PriorityManagement});
	softwareModulesFolder.open();

	var personalFolder = gui.addFolder("Personal Devices");
	personalFolder.add(personalParameters, 'Tablet').name('Tablet').listen().onChange(()=>onProducerStateChange('Tablet'));
	personalFolder.add(personalParameters, 'Smartphone').name('Smartphone').listen().onChange(()=>onProducerStateChange('Smartphone'));
	personalFolder.add(personalParameters, 'EEGHeadset').name('EEGHeadset').listen().onChange(()=>onProducerStateChange('EEGHeadset'));
	personalFolder.add(personalParameters, 'EyeTracker').name('EyeTracker').listen().onChange(()=>onProducerStateChange('EyeTracker'));
	personalFolder.add(personalParameters, 'Vuzix').name('Vuzix').listen().onChange(()=>onProducerStateChange('Vuzix'));
	personalFolder.add(personalParameters, 'HTCVive').name('HTCVive').listen().onChange(()=>onProducerStateChange('HTCVive'));
	personalFolder.add(personalParameters, 'HoloLens').name('HoloLens').listen().onChange(()=>onProducerStateChange('HoloLens'));
	function onProducerStateChange(deviceName) {
		
		
		if (deviceName === "Vicon") {
			producersParameters[deviceName].onOff = true;
			return;
		}
		if (deviceName === "Smartphone" || deviceName == "Wall Display")
		{
			producersParameters[deviceName].onOff = !producersParameters[deviceName].onOff;
			phone.material.visible = producersParameters[deviceName].onOff;
			producersParameters[deviceName].onOff = phone.material.visible;
		}
		if (deviceName === "Tablet") {
			PriorityManagement.PriorityLevels["Tablet"] = producersParameters[deviceName].onOff ? "medium" : "none";
			producersParameters[deviceName].onOff = !producersParameters[deviceName].onOff;
			tablet.material.visible = producersParameters[deviceName].onOff;
			// result = scene.children.findIndex(child => child.name == "HTCController1"); 
			// if (result>0) scene.children[result].visible = !tablet.material.visible;  //producersParameters["HTCController1"].onOff = scene.children[result].visible;}
			// result = scene.children.findIndex(child => child.name == "HTCController2");
			// if (result>0) scene.children[result].visible = !tablet.material.visible; //producersParameters["HTCController2"].onOff = scene.children[result].visible;}
			// result = scene.children.findIndex(child => child.name == "HTCVive");
			// if (result>0) {scene.children[result].visible = !tablet.material.visible; producersParameters["HTCVive"].onOff = scene.children[result].visible;}
			if (producersParameters[deviceName].onOff) {
				Vicon.addEntity("Tablet");
			}
			else {
				Vicon.removeEntity("Tablet");
			}
		}
		result = scene.children.findIndex(child => child.name == deviceName);
		if(result > 0) {
			console.log(deviceName);
			
			scene.children[result].visible = !scene.children[result].visible;
			producersParameters[deviceName].onOff = scene.children[result].visible;
			
		}
			
		if (deviceName === "HTCVive"){
			//tablet.material.visible = !producersParameters[deviceName].onOff ;
			//producersParameters["Tablet"].onOff = tablet.material.visible;
			result = scene.children.findIndex(child => child.name == "HTCController1");
			if(result > 0) scene.children[result].visible = producersParameters[deviceName].onOff;
			result = scene.children.findIndex(child => child.name == "HTCController2");
			if(result > 0) scene.children[result].visible = producersParameters[deviceName].onOff;
			
		}
		
	}
	personalFolder.open();

	var ambientFolder = gui.addFolder("Ambient Devices");
	ambientFolder.add(ambientParameters, 'Vicon').name('Vicon').listen().onChange(()=>onProducerStateChange('Vicon'));
	ambientFolder.add(ambientParameters, 'Microphone').name('Microphone').listen().onChange(()=>onProducerStateChange('Microphone'));
	ambientFolder.add(ambientParameters, 'WallDisplay').name('Wall Display').listen().onChange(()=>onTvStateChange());
	onTvStateChange();
	function onTvStateChange() {
		if (ambientParameters.WallDisplay)
			ambientParameters.WallDisplay = false;
		else
			ambientParameters.WallDisplay = true;
		tv.material.visible = ambientParameters.WallDisplay;
		PriorityManagement.PriorityLevels["WallDisplay"] = ambientParameters.WallDisplay ? "medium" : "none";
		for (let param in phoneParameters) {
			if (phoneParameters[param]) {
				onPhoneMessage(param);
			}
		}
	}
	ambientFolder.add(ambientParameters, 'Surround').name('Surround sound system').listen().onChange(()=>onSoundStateChange());
	onSoundStateChange();
	function onSoundStateChange() {
		PriorityManagement.PriorityLevels["SoundSystem"] = ambientParameters.Surround ? "low" : "none";
		if (ambientParameters.Surround) {
			let audio = dom("mediaSound");
			if (phoneParameters.highPriorityMessage) {
				sound1.material.color.setHex("0xff0000");
				sound2.material.color.setHex("0xff0000");
				audio.src = "media/important.ogg";
				audio.load();
				audio.play();
			}
			else {
				sound1.material.color.setHex("0x0000ff");
				sound2.material.color.setHex("0x0000ff");
				if (audio.src.indexOf("song.ogg") === -1) {
					audio.src = "media/song.ogg";
					audio.load();
				}
				if (audio.readyState >= 2) {
					audio.play();
				}
			}
		}
		else {
			sound1.material.color.setHex("0x222222");
			sound2.material.color.setHex("0x222222");
			dom("mediaSound").pause();
		}
	}
	ambientFolder.open();
	let devicesElements = $("#jsonDevicesNames").children();
	for (let i = 0; i < devicesElements.length; i++) {
		let device = devicesElements[i];
		let deviceName = device.getAttribute("data-device-name");

		let deviceInitialState = device.getAttribute("data-device-initial-state");
		producersParameters[deviceName] = { onOff: (deviceInitialState === "true") };
		onProducerStateChange(deviceName);
	}
	personalParameters.Tablet = false;
	producersParameters["Tablet"].OnOff = false;
	tablet.material.visible = false;

	var XRObjects = gui.addFolder("XR Virtual Devices");
	XRObjects.add(XRObjectsParameters, 'MixedReality').name("Mixed Reality").listen().onChange(()=>onXRObjects("Mixed Reality"));
	XRObjects.add(XRObjectsParameters, 'AugmentedReality').name("Augmented Reality").listen().onChange(()=>onXRObjects("Augmented Reality"));
	XRObjects.add(XRObjectsParameters, 'VirtualReality').name("Virtual Reality").listen().onChange(()=>onXRObjects("Virtual Reality"));
	XRObjects.add(XRObjectsParameters, 'SharedObject').name("Public").listen().onChange(()=>onXRObjects("Shared Object"));
	XRObjects.open();
	 myVar = setInterval(MR, 2000);
	myVar1 = setInterval(ARRR, 5000);
	myVar2 = setInterval(VRR, 3500);
}
function MR(){
	var name = "Mixed Reality";
	var interm = scene.children.findIndex(child => child.name === "HoloLens");
	if (scene.children[interm].visible == false)
		return;
		
	var rrresult = scene.children.findIndex(child => child.name === name);
	if (XRObjectsParameters.MixedReality) 
		XRObjectsParameters.MixedReality = false;
	else 
		XRObjectsParameters.MixedReality = true;
	if (rrresult) 
		scene.children[rrresult].visible = XRObjectsParameters.MixedReality;
}
function ARRR(){
	var name = "Augmented Reality";
	var interm = scene.children.findIndex(child => child.name === "Vuzix");
		if (scene.children[interm].visible == false)
			return;
	var rrresult = scene.children.findIndex(child => child.name === name);
	if (XRObjectsParameters.AugmentedReality) 
		XRObjectsParameters.AugmentedReality = false;
	else 
		XRObjectsParameters.AugmentedReality = true;
	if (rrresult) 
		scene.children[rrresult].visible = XRObjectsParameters.AugmentedReality;
}
function VRR(){
	
	var interm = scene.children.findIndex(child => child.name === "HTCVive");
		if (scene.children[interm].visible == false)
			return;
	if (XRObjectsParameters.VirtualReality) 
		XRObjectsParameters.VirtualReality = false;
	else 
		XRObjectsParameters.VirtualReality = true;
	var name = "Virtual Reality";
	var rrresult = scene.children.findIndex(child => child.name === name);
	if (rrresult) 
		scene.children[rrresult].visible = XRObjectsParameters.VirtualReality;
	name = "Sandra1";
	rrresult = scene.children.findIndex(child => child.name === name);
	if (rrresult) 
		scene.children[rrresult].visible = XRObjectsParameters.VirtualReality;
	name = "Sandra2";
	rrresult = scene.children.findIndex(child => child.name === name);
	if (rrresult) 
		scene.children[rrresult].visible = XRObjectsParameters.VirtualReality;
}
function onXRObjects(name) {
	var rrresult = scene.children.findIndex(child => child.name === name);
	if (name === "Mixed Reality") {
	var interm = scene.children.findIndex(child => child.name === "HoloLens");
	if (scene.children[interm].visible == false)
		return;
		if (XRObjectsParameters.MixedReality) 
			XRObjectsParameters.MixedReality = true;
		else 
			XRObjectsParameters.MixedReality = false;
		if (rrresult) 
			scene.children[rrresult].visible = XRObjectsParameters.MixedReality;
	}
	else if (name === "Augmented Reality")
	{
		var interm = scene.children.findIndex(child => child.name === "Vuzix");
		if (scene.children[interm].visible == false)
			return;
		if (XRObjectsParameters.AugmentedReality) 
			XRObjectsParameters.AugmentedReality = true;
		else 
			XRObjectsParameters.AugmentedReality = false;
		if (rrresult) 
			scene.children[rrresult].visible = XRObjectsParameters.AugmentedReality;
	}
	else if (name === "Virtual Reality")
	{
		var interm = scene.children.findIndex(child => child.name === "HTCVive");
		if (scene.children[interm].visible == false)
			return;
		if (XRObjectsParameters.VirtualReality) 
			XRObjectsParameters.VirtualReality = true;
		else 
			XRObjectsParameters.VirtualReality = false;
		if (rrresult) 
			scene.children[rrresult].visible = XRObjectsParameters.VirtualReality;
	}
	else if (name === "Shared Object")
	{
		if (XRObjectsParameters.SharedObject) 
			XRObjectsParameters.SharedObject = true;
		else 
			XRObjectsParameters.SharedObject = false;
		if (result) {
			console.log(name);
			scene.children[rrresult].visible = XRObjectsParameters.SharedObject;
		}
			
	}
}
function onWindowResize() {
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
	renderer.setSize(window.innerWidth, window.innerHeight);
}
function animate() {
	requestAnimationFrame(animate);
	controls.update();
	renderer.render(scene, camera);
	cssRenderer.render(scene, camera);
}
function createCssRenderer() {
	var cssRenderer = new THREE.CSS3DRenderer();
	cssRenderer.setSize(window.innerWidth, window.innerHeight);
	return cssRenderer;
}

function onDocumentMouseDown( event ) 
{
	// the following line would stop any other event handler from firing
	// (such as the mouse's TrackballControls)
	// event.preventDefault();
	
	if (!isPopulated){
		var result = scene.children.findIndex(child => child.name === "Michael"); 
		if (result>0) arrayXR.push(scene.children[result]);
		result = scene.children.findIndex(child => child.name === "Matthew"); 
		if (result>0) arrayXR.push(scene.children[result]);
		result = scene.children.findIndex(child => child.name === "Woman"); 
		if (result>0) arrayXR.push(scene.children[result]);
		//console.log(arrayXR);
		isPopulated = true;
	}
	// update the mouse variable
	mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
	mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;

	// create a Ray with origin at the mouse position
	//   and direction into the scene (camera direction)
	var vector = new THREE.Vector3( event.ScreenX,event.ScreenY, 1 );
	projector.unprojectVector( vector, camera );
	var ray = new THREE.Raycaster( camera.position, vector.sub( camera.position ).normalize() );

	// create an array containing all objects in the scene with which the ray intersects
	
	var intersects = ray.intersectObjects( arrayXR );
	console.log('Intersects:');
	console.log(intersects);
	// if there is one (or more) intersections
	if ( intersects.length > 0 )
	{
		console.log("Hit @ " + toString( intersects[0].point ) );
		// change the color of the closest face.
		intersects[ 0 ].face.color.setRGB( 0.8 * Math.random() + 0.2, 0, 0 ); 
		intersects[ 0 ].object.geometry.colorsNeedUpdate = true;
	}

}
async function startSimulation() {
	let devicesElements = $("#jsonDevicesNames").children();
	// create producer devices
	for (let i = 0; i < 9; i++) {
		let device = devicesElements[i];
		let deviceName = device.getAttribute("data-device-name");
		let deviceIp = device.getAttribute("data-device-ip");
		if (deviceName == "Tablet") {
			let tabletEvents = [];
			tabletEvents.push({ deviceName: "InterruptibilityModule", eventName: "onNotification" });
			producers[deviceName] = new producersClasses[deviceName](EUPHORIA_OUTPUT_ADDRESS, EUPHORIA_INPUT_ADDRESS, deviceName, deviceIp, tabletEvents);
		}
		else {
			producers[deviceName] = new producersClasses[deviceName](EUPHORIA_INPUT_ADDRESS, deviceName, deviceIp);
		}
		if (deviceName === "Vicon") {
			Vicon.addEntity("Sandra");
			Vicon.addEntity("WallDisplay");
			Vicon.addEntity("Smartphone");
		}
		console.log(deviceName);
		//console.log(producers[deviceName].producer);
		producers[deviceName].producer.interval = device.getAttribute("data-device-delay");
		producers[deviceName].start();
	}

	// create AttentionDetectionModule
	let attentionDetectionDevicesNamesAndEvents = [];
	attentionDetectionDevicesNamesAndEvents.push({ deviceName: "Vicon", eventName: "onMotionTrackingData" });
	attentionDetectionDevicesNamesAndEvents.push({ deviceName: "Tablet", eventName: "onTouchDown" });
	attentionDetectionDevicesNamesAndEvents.push({ deviceName: "Smartphone", eventName: "onNotification" });
	attentionDetectionDevicesNamesAndEvents.push({ deviceName: "HoloLens", eventName: "onNotification" });
	attentionDetectionDevicesNamesAndEvents.push({ deviceName: "Vuzix", eventName: "onNotification" });
	attentionDetectionDevicesNamesAndEvents.push({ deviceName: "HTCVive", eventName: "onNotification" });

	attentionDetectionModule = new AttentionModule(
		EUPHORIA_OUTPUT_ADDRESS,
		EUPHORIA_INPUT_ADDRESS,
		"192.168.0.8",
		attentionDetectionDevicesNamesAndEvents
	);
	attentionDetectionModule.start();

	// create ContextAwareness
	let contextAwarenessDevicesNamesAndEvents = [];
	contextAwarenessDevicesNamesAndEvents.push({ deviceName: "Vicon", eventName: "onMotionTrackingData" });
	contextAwarenessDevicesNamesAndEvents.push({ deviceName: "Tablet", eventName: "onTouchDown" });
	contextAwarenessDevicesNamesAndEvents.push({ deviceName: "Smartphone", eventName: "onNotification" });
	contextAwarenessDevicesNamesAndEvents.push({ deviceName: "Microphone", eventName: "onRecording" });
	contextAwarenessDevicesNamesAndEvents.push({ deviceName: "EyeTracker", eventName: "onGazing" });
	contextAwarenessDevicesNamesAndEvents.push({ deviceName: "Vuzix", eventName: "onData" });
	contextAwarenessDevicesNamesAndEvents.push({ deviceName: "HoloLens", eventName: "onData" });
	attentionDetectionDevicesNamesAndEvents.push({ deviceName: "HTCVive", eventName: "onNotification" });
	contextAwarenessDevicesNamesAndEvents.push({ deviceName: "AttentionModule", eventName: "onAttentionFocus" });

	contextAwarenessModule = new ContextAwareness(
		EUPHORIA_OUTPUT_ADDRESS,
		EUPHORIA_INPUT_ADDRESS,
		"192.168.0.8",
		contextAwarenessDevicesNamesAndEvents
	);
	contextAwarenessModule.start();

	// create PriorityManagement
	let priorityManagementDevicesNamesAndEvents = [];
	priorityManagementDevicesNamesAndEvents.push({ deviceName: "ContextAwareness", eventName: "onContextChange" });
	priorityManagementModule = new PriorityManagement(
		EUPHORIA_OUTPUT_ADDRESS,
		EUPHORIA_INPUT_ADDRESS,
		"192.168.0.8",
		priorityManagementDevicesNamesAndEvents
	);
	priorityManagementModule.start();

	// create InterruptibilityPrediction
	let interruptibilityPredictionDevicesNamesAndEvents = [];
	interruptibilityPredictionDevicesNamesAndEvents.push({ deviceName: "PriorityManagement", eventName: "onPriorityChange" });
	interruptibilityPredictionModule = new InterruptibilityPrediction(
		EUPHORIA_OUTPUT_ADDRESS,
		EUPHORIA_INPUT_ADDRESS,
		"192.168.0.8",
		interruptibilityPredictionDevicesNamesAndEvents
	);
	interruptibilityPredictionModule.start();

	// create generic consumer for simulation
	let simulationDevicesNamesAndEvents = [];
	simulationDevicesNamesAndEvents.push({ deviceName: "Vicon", eventName: "onMotionTrackingData" });
	simulationDevicesNamesAndEvents.push({ deviceName: "Tablet", eventName: "onTouchDown" });
	simulationDevicesNamesAndEvents.push({ deviceName: "Microphone", eventName: "onRecording" });
	simulationDevicesNamesAndEvents.push({ deviceName: "Smartphone", eventName: "onNotification" });
	simulationDevicesNamesAndEvents.push({ deviceName: "EEGHeadset", eventName: "onEmotion" });
	simulationDevicesNamesAndEvents.push({ deviceName: "EyeTracker", eventName: "onGazing" });
	simulationDevicesNamesAndEvents.push({ deviceName: "Vuzix", eventName: "onNotification" });
	simulationDevicesNamesAndEvents.push({ deviceName: "HoloLens", eventName: "onNotification" });
	simulationDevicesNamesAndEvents.push({ deviceName: "HTCVive", eventName: "onNotification" });
	simulationDevicesNamesAndEvents.push({ deviceName: "AttentionModule", eventName: "onAttentionFocus" });
	simulationDevicesNamesAndEvents.push({ deviceName: "ContextAwareness", eventName: "onContextChange" });
	simulationDevicesNamesAndEvents.push({ deviceName: "PriorityManagement", eventName: "onPriorityChange" });
	simulationDevicesNamesAndEvents.push({ deviceName: "InterruptibilityPrediction", eventName: "onInterruption" });

	consumer = new EuphoriaConsumer(EUPHORIA_OUTPUT_ADDRESS, simulationDevicesNamesAndEvents, onMessage);
	consumer.start();

	function onMessage(jsonData) {
		let key = typeof (jsonData.header.deviceName) !== 'undefined' ? jsonData.header.deviceName : jsonData.header.moduleName;
		if (producersParameters[key] === undefined || producersParameters[key].onOff) {
			messages[key] = jsonData;
			$("#jsonPanelCode").JSONView(messages[activeMessageType]);
		}
	}
	showJson($("#jsonDevicesNames").children()[0]);
	showCode($("#jsFilesNames").children()[0]);
}
function showCode(navElement) {
	let menuItems = $("#jsFilesNames").children();
	for (let i = 0; i < menuItems.length; i++) {
		menuItems[i].className = "";
	}
	navElement.className = "isNavActive";

	let jsCode = loadFile("js/sapiens/" + navElement.getAttribute("data-file-name"));
	$("#jsPanelCode").html("<pre><code class='javascript'>" + jsCode + "</code></pre>");
	document.querySelectorAll('pre code').forEach((block) => {
		hljs.highlightBlock(block);
	});
}
function showJson(navElement) {
	let menuItems = $("#jsonDevicesNames").children();
	for (let i = 0; i < menuItems.length; i++) {
		menuItems[i].className = "";
	}
	navElement.className = "isNavActive";
	activeMessageType = navElement.getAttribute("data-device-name");
}
function loadFile(filePath) {
	var result = null;
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.open("GET", filePath, false);
	xmlhttp.send();
	if (xmlhttp.status == 200) {
		result = xmlhttp.responseText;
	}
	return result;
}


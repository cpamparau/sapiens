class InterruptibilityPrediction {
    constructor(euphoriaOutputURL, euphoriaInputURL, IP, events) {
        this.consumer = new EuphoriaConsumer(euphoriaOutputURL, events, this.onMessageRead.bind(this));
        this.producer = new EuphoriaProducer(euphoriaInputURL, "InterruptibilityPrediction", "InterruptibilityPrediction", IP, this.onMessageWrite, true);
        this.message = this.producer.message;
        this.onMessageWrite = this.onMessageWrite.bind(this);
    }
    onMessageRead(message) {
        //this.message.body.received = message.header.eventName;
        this.producer.onMessageWrite();
        this.onMessageWrite();
        this.producer.sendMessage();
    }
    onMessageWrite() {
        this.message.header.eventName = "onInterruption";
        this.message.body = InterruptibilityPrediction.notificationDevices;
    }
    start() {
        this.consumer.start();
        this.producer.start();
        this.producer.pause();
    }
    stop() {
        this.consumer.stop();
        this.producer.stop();
    }
}

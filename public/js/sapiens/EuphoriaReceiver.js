class EuphoriaReceiver {
    constructor() {
        this.webSocket = null;
    }
    connect(serverUrl, onMessageCallback, messageContext) {
        let _this = this;
        return new Promise(function (resolve, reject) {
            _this.disconnect();
            _this.webSocket = new WebSocket(serverUrl);
            _this.webSocket.onopen = function () {
                resolve();
            }
            _this.webSocket.onerror = function (error) {
                reject();
            }
            _this.webSocket.onmessage = function (event) {
                onMessageCallback.call(messageContext, event.data);
            }
        });
    }
    disconnect() {
        if (this.webSocket !== undefined && this.webSocket !== null) {
            this.webSocket.close();
        }
    }
}
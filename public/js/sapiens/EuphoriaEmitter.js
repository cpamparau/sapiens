class EuphoriaEmitter {
    constructor() {
        this.webSocket = null;
        this.connect = this.connect.bind(this);
    }
    connect(serverUrl) {
        let _this = this;
        return new Promise(function (resolve, reject) {
            _this.disconnect();
            _this.webSocket = new WebSocket(serverUrl);
            _this.webSocket.onopen = function () {
                resolve();
            }
            _this.webSocket.onerror = function (error) {
                reject();
            }
        });
    }
    disconnect() {
        if (this.webSocket !== undefined && this.webSocket !== null) {
            this.webSocket.close();
        }
    }
    sendMessage(jsonObject) {
        if (this.webSocket.readyState === this.webSocket.OPEN) {
            this.webSocket.send(JSON.stringify(jsonObject));
        }
        else {
            throw new Error();
        }
    }
}
class Smartphone {
    constructor(euphoriaURL, deviceName, deviceIP) {
        this.producer = new EuphoriaProducer(euphoriaURL, "PersonalTouchscreen", deviceName, deviceIP, this.onData);
        this.message = this.producer.message;

        this.onData = this.onData.bind(this);
    }
    onData() {
        this.message.body.notificationContent = "";
        this.message.header.eventName = "onNotification";
    }
    setNotificationPriority(notificationPriority) {
        this.message.body.notificationPriority = notificationPriority;
    }
    start() {
        this.producer.start();
        this.producer.pause();
    }
    stop() {
        this.producer.stop();
    }
    pause() {
        this.producer.pause();
    }
    resume() {
        this.producer.resume();
        console.log("qqqq");
    }
}
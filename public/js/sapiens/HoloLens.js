class HoloLens {
    constructor(euphoriaURL, deviceName, deviceIP, events) {
        this.producer = new EuphoriaProducer(euphoriaURL, "HoloLens", deviceName, deviceIP, this.onData);
        this.message = this.producer.message;
        this.onData = this.onData.bind(this);
    }
    onData(message){
		this.message.body.notificationContent = "";
        this.message.header.eventName = "onNotification";
	}
    start() {
        this.producer.start();
    }
    stop() {
        this.producer.stop();
    }
    pause() {
        this.producer.pause();
    }
    resume() {
        this.producer.resume();
    }
}
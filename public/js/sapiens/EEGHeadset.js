class EEGHeadset {
    constructor(euphoriaURL, deviceName, deviceIP) {
        this.producer = new EuphoriaProducer(euphoriaURL, "EEGHeadset", deviceName, deviceIP, this.onData);
        this.message = this.producer.message;
        this.onData = this.onData.bind(this);
    }
    onData() {
        const EXCITEMENT_MIN = 0;
        const EXCITEMENT_MAX = 1;
        const MEDITATION_MIN = 0;
        const MEDITATION_MAX = 1;
        const FRUSTRATION_MIN = 0;
        const FRUSTRATION_MAX = 1;
        this.message.header.eventName = "onEmotion";
        this.message.body.longTermExcitementScore = getRndFloat(EXCITEMENT_MIN, EXCITEMENT_MAX);
        this.message.body.meditationScore = getRndFloat(MEDITATION_MIN, MEDITATION_MAX);
        this.message.body.frustrationScore = getRndFloat(FRUSTRATION_MIN, FRUSTRATION_MAX);
        this.message.body.boredomScore = 1 - this.message.body.longTermExcitementScore;
    }
    start() {
        this.producer.start();
    }
    stop() {
        this.producer.stop();
    }
    pause() {
        this.producer.pause();
    }
    resume() {
        this.producer.resume();
    }

}
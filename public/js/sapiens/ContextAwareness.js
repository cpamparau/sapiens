class ContextAwareness {
    constructor(euphoriaOutputURL, euphoriaInputURL, IP, events) {
        this.consumer = new EuphoriaConsumer(euphoriaOutputURL, events, this.onMessageRead.bind(this));
        this.producer = new EuphoriaProducer(euphoriaInputURL, "ContextAwareness", "ContextAwareness", IP, this.onMessageWrite, true);
        this.ttl = {};
        this.message = this.producer.message;
        this.message.body.entities = {};
        setInterval(() => {
            for (const deviceName in this.ttl) {
                if (this.ttl[deviceName] -= ContextAwareness.DELTA_TTL <= 0) {
                    this.message.body.entities[deviceName] = {};
                }
            }
        }, ContextAwareness.DELTA_TTL);
        this.onMessageWrite = this.onMessageWrite.bind(this);
    }
    onMessageRead(message) {
        let deviceName = message.header.deviceName || message.header.moduleName;
        this.ttl[deviceName] = ContextAwareness.DEFAULT_TTL;
        if (typeof (this.message.body.entities[deviceName]) === "undefined" && deviceName !== "Vicon" && deviceName !== "AttentionModule") {
            this.message.body.entities[deviceName] = {};
        }
        switch (deviceName) {
            case "Tablet":
            case "Microphone":
            case "Smartphone":
            case "EyeTracker":
			case "Vuzix":
			case "HoloLens":
                this.message.body.entities[deviceName].data = message.body;
                break;
            case "Vicon":
                for (const entity in message.body.entities) {
                    this.ttl[entity] = ContextAwareness.DEFAULT_TTL;
                    if (typeof (this.message.body.entities[entity]) === "undefined") {
                        this.message.body.entities[entity] = {};
                    }
                    this.message.body.entities[entity].location = message.body.entities[entity].location;
                    this.message.body.entities[entity].orientation = message.body.entities[entity].orientation;
                }
                break;
            case "AttentionModule":
                if (message.body.entityFocus !== "_") {
                    this.message.body.entities[message.body.entityFocus].hasFocus = {};
                    this.message.body.entities[message.body.entityFocus].probability = message.body.probability;
                    this.message.body.entities[message.body.entityFocus].proactivity = message.body.proactivity;
                }
                break;
        }
        this.producer.onMessageWrite();
        this.onMessageWrite();
        this.producer.sendMessage();
    }
    onMessageWrite() {
        this.message.header.eventName = "onContextChange";
    }
    start() {
        this.consumer.start();
        this.producer.start();
        this.producer.pause();
    }
    stop() {
        this.consumer.stop();
        this.producer.stop();
    }
}

ContextAwareness.MAX_TTL = 3000;// ttl - time to live [ms]
ContextAwareness.DELTA_TTL = 3000;
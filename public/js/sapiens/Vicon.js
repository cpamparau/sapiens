class Vicon {
    constructor(euphoriaURL, deviceName, deviceIP) {
        this.producer = new EuphoriaProducer(euphoriaURL, "MotionTracker", deviceName, deviceIP, this.onData);
        this.message = this.producer.message;
        this.message.body.entities = {};
        this.onData = this.onData.bind(this);
    }
    onData() {
        this.message.header.eventName = "onMotionTrackingData";
        for (let i = 0; i < Vicon.entitiesNames.length; i++) {
            let entityName = Vicon.entitiesNames[i];
            this.message.body.entities[entityName] = {};
            //this.message.body.entities[entityName].name = Vicon.entitiesNames[i];
            this.message.body.entities[entityName].location = {};
            this.message.body.entities[entityName].location.x = getRndInteger(-1500, 1500);
            this.message.body.entities[entityName].location.y = getRndInteger(0, 3000);
            this.message.body.entities[entityName].location.z = getRndInteger(800, 1600);
            this.message.body.entities[entityName].orientation = {};
            this.message.body.entities[entityName].orientation.yaw = getRndFloat(-Math.PI / 4, Math.PI / 4);
            this.message.body.entities[entityName].orientation.pitch = getRndFloat(-Math.PI / 4, Math.PI / 4);
            this.message.body.entities[entityName].orientation.roll = getRndFloat(-Math.PI / 4, Math.PI / 4);
        }
    }
    static addEntity(entityName) {
        let i;
        for (i = 0; i < Vicon.entitiesNames.length; i++) {
            if (Vicon.entitiesNames[i] === entityName) {
                break;
            }
        }
        if (i == Vicon.entitiesNames.length) {
            Vicon.entitiesNames.push(entityName);
        }
    }
    static removeEntity(entityName) {
        let i;
        for (i = 0; i < Vicon.entitiesNames.length; i++) {
            if (Vicon.entitiesNames[i] === entityName) {
                break;
            }
        }
        if (i <= Vicon.entitiesNames.length) {
            Vicon.entitiesNames.splice(i, 1);
        }
    }
    static getFocus(message) {
        // For the purpose of this simulation,
        // we generate the entity with the focus of attention at random.
        let nameIndex = Math.floor(Math.random() * Vicon.entitiesNames.length);
        return nameIndex === 0 ? "_" : Vicon.entitiesNames[nameIndex];
    }
    start() {
        this.producer.start();
    }
    stop() {
        this.producer.stop();
    }
    pause() {
        this.producer.pause();
    }
    resume() {
        this.producer.resume();
    }
}
Vicon.entitiesNames = [];
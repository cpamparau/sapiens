class XRDisplayHandler{
	XRDisplay vector[];
	createXRDisplay(castShadow, receiveShadow, 
		PosX, PosY, PosZ, RotX, RotY, RotZ, 
		name, IsVisible, color, type, size){
		XRDisplay xrdisplay;
		xrdisplay.castShadow = castShadow;
		xrdisplay.receiveShadow = receiveShadow;
		xrdisplay.position.x = PosX;
		xrdisplay.position.y = PosY;
		xrdisplay.position.z = PosZ;
		xrdisplay.name = name;
		xrdisplay.content.color = color;
		xrdisplay.content.type = type;
		xrdisplay.content.size = size;
		if (RotX != 2*Math.PI) 
			xrdiaplay.rotation.x = RotX;
		if (RotY != 2*Math.PI) 
			xrdiaplay.rotation.Y = RotY;
		if (RotZ != 2*Math.PI) 
			xrdiaplay.rotation.z = RotZ;
		xrdisplay.material.visible = IsVisible;
		scene.add(xrdisplay);
		this.vector.push(xrdisplay);
	}
	destroyXRDisplay(){
		scene.remove(xrdisplay);
		this.vector.remove(xrdisplay);
	}
}


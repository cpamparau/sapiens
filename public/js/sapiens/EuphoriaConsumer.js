class EuphoriaConsumer {
    constructor(URL, devicesNamesAndEvents, onServerMessageCallback) {
        this.receiver = new EuphoriaReceiver();
        this.URL = URL;
        this.devicesNamesAndEvents = devicesNamesAndEvents;
        this.deviceName = "";
        this.eventName = "";
        this.isRunning = false;
        this.onServerMessageCallback = onServerMessageCallback;
    }
    async start() {
        this.isRunning = true;
        await this.connect();
    }
    stop() {
        this.isRunning = false;
        this.disconnect();
    }
    pause() {
        this.isRunning = false;
    }
    resume() {
        this.isRunning = true;
    }
    async connect() {
        try {
            let url = this.URL + "?deviceName=" + this.deviceName + "&eventName=" + this.eventName;
            await this.receiver.connect.call(this.receiver, url, this.onServerMessage, this);
        }
        catch (err) {
            throw err;
        }
    }
    onServerMessage(data) {
        if (this.isRunning) {
            let jsonData = JSON.parse(data);
            for (let i = 0; i < this.devicesNamesAndEvents.length; i++) {
                if ((this.devicesNamesAndEvents[i].deviceName == jsonData.header.deviceName || this.devicesNamesAndEvents[i].deviceName == jsonData.header.moduleName) && this.devicesNamesAndEvents[i].eventName == jsonData.header.eventName) {
                    this.onServerMessageCallback(jsonData);
                    break;
                }
            }
        }
    }
    disconnect() {
        this.receiver.disconnect();
    }
}

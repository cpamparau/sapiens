class Microphone {
    constructor(euphoriaURL, deviceName, deviceIP) {
        this.producer = new EuphoriaProducer(euphoriaURL, "Microphone", deviceName, deviceIP, this.onData);
        this.message = this.producer.message;
        this.onData = this.onData.bind(this);
    }
    onData() {
        const BUFFER_LENGTH = 16;
        const CHAR_SET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        this.message.header.eventName = "onRecording";
        this.message.body.dataBuffer = [];
        for (let i = 0; i < BUFFER_LENGTH; i++) {
            let randomPoz = Math.floor(Math.random() * CHAR_SET.length);
            this.message.body.dataBuffer.push(CHAR_SET.substring(randomPoz, randomPoz + 1).charCodeAt(0).toString(16));
        }
    }
    start() {
        this.producer.start();
    }
    stop() {
        this.producer.stop();
    }
    pause() {
        this.producer.pause();
    }
    resume() {
        this.producer.resume();
    }

}
class Tablet {
    constructor(outputURL, inputURL, deviceName, deviceIP, events) {
        this.consumer = new EuphoriaConsumer(outputURL, events, this.onMessageRead.bind(this));
        this.producer = new EuphoriaProducer(inputURL, "PersonalTouchscreen", deviceName, deviceIP, this.onMessageWrite);
        this.message = this.producer.message;
        this.onMessageWrite = this.onMessageWrite.bind(this);
    }
    onMessageWrite() {
        this.message.header.eventName = "onTouchDown";
        this.message.body.location = {};
        this.message.body.location.x = getRndInteger(0, 480);
        this.message.body.location.y = getRndInteger(0, 640);
    }
    onMessageRead(message) {
        if (message.header.eventName === "onNotification") {
            this.showNotification(message.body.modality, message.body.content, message.body.priority);
        }
    }
    showNotification(modality, content, priority) {
        // call of device specific API
    }
    start() {
        this.producer.start();
    }
    stop() {
        this.producer.stop();
    }
    pause() {
        this.producer.pause();
    }
    resume() {
        this.producer.resume();
    }
}
class EyeTracker {
    constructor(euphoriaURL, deviceName, deviceIP) {
        this.producer = new EuphoriaProducer(euphoriaURL, "EyeTracker", deviceName, deviceIP, this.onData);
        this.message = this.producer.message;
        this.onData = this.onData.bind(this);
    }
    onData() {
        const MIN_GAZE_X = -100;
        const MAX_GAZE_X = 1200;
        const MIN_GAZE_Y = -100;
        const MAX_GAZE_Y = 800;
        this.message.header.eventName = "onGazing";
        this.message.body.left_gaze_x = getRndInteger(MIN_GAZE_X, MAX_GAZE_X);
        this.message.body.left_gaze_y = getRndInteger(MIN_GAZE_Y, MAX_GAZE_Y);
        this.message.body.right_gaze_x = this.message.body.left_gaze_x;
        this.message.body.right_gaze_y = this.message.body.left_gaze_y;
    }
    start() {
        this.producer.start();
    }
    stop() {
        this.producer.stop();
    }
    pause() {
        this.producer.pause();
    }
    resume() {
        this.producer.resume();
    }

}
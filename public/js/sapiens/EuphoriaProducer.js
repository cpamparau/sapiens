class EuphoriaProducer {
    constructor(URL, highLevelDevice, deviceName, deviceIP, onData, isModule) {
        this.emitter = new EuphoriaEmitter();
        this.URL = URL;
        this.webSocket = null;
        this.message = {};
        this.message.header = {};
        if (!isModule) {
            this.message.header.deviceName = deviceName;
            this.message.header.highLevelDevice = highLevelDevice;
            this.message.header.deviceIP = deviceIP;
        }
        else {
            this.message.header.moduleName = deviceName;
            this.message.header.highLevelModule = highLevelDevice;
            this.message.header.moduleIP = deviceIP;
        }
        this.message.header.eventName = "";
        this.message.body = {};
        this.onData = onData;
        this.interval = AVG_INTERVAL;
        this.timer = null;
        this.isTimerRunning = false;
        this.connect = this.connect.bind(this);
        this.onMessageWrite = this.onMessageWrite.bind(this);
    }
	
    async start() {
        this.isTimerRunning = true;
        await this.connect();
        this.timer = setInterval(
           () => {
                if (this.isTimerRunning) {
					
                    this.onMessageWrite();
					this.onData();
                    this.sendMessage();
					let interval = poissonProcess.sample(AVG_INTERVAL);
					//clearInterval(this.timer);
					this.timer = setInterval(this.timer, interval);
					//console.log("[request method] interval, "+ interval);
                }
            },
           this.interval
        );
    }
    onMessageWrite() {
        //this.message.header.timestamp = new Date().getTime();
    }
    stop() {
        this.isTimerRunning = false;
        clearInterval(this.timer);
        this.disconnect();
    }
    pause() {
        this.isTimerRunning = false;
    }
    resume() {
        this.isTimerRunning = true;
    }
    async connect() {
        try {
            await this.emitter.connect(this.URL);
        }
        catch (err) {
            throw err;
        }
    }
    sendMessage() {
        try {
            this.emitter.sendMessage(this.message);
        }
        catch (err) {
            console.log(err);
        }
    }
    disconnect() {
        this.emitter.disconnect();
    }
}
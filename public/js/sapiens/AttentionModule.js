class AttentionModule {
    constructor(outputURL, inputURL, IP, events) {
        this.consumer = new EuphoriaConsumer(outputURL, events, this.onMessageRead.bind(this));
        this.producer = new EuphoriaProducer(inputURL, "AttentionModule", "AttentionModule", IP, this.onMessageWrite, true);
        this.entityFocus = "";	// computes the entity that has the focus of attention
        this.message = this.producer.message; // message to be sent to the Engine
        this.onMessageWrite = this.onMessageWrite.bind(this);
        setInterval(() => {
            for (var key in AttentionModule.proactivity)
                AttentionModule.proactivity[key] *= 0.9; // non-linear decrease in time
        }, 1000);
    }

    /// Handles messages from I/O devices
    onMessageRead(message) {
        switch (message.header.eventName) {
            case "onTouchDown": this.entityFocus = message.header.deviceName; break;
            case "onMotionTrackingData": this.entityFocus = Vicon.getFocus(message); break;
        }

        // compute probabilities of focus of attention for each device
        if (AttentionModule.probability[this.entityFocus] === undefined ||
            AttentionModule.probability[this.entityFocus] <
            AttentionModule.PROB_TRESHOLD)
            AttentionModule.probability[this.entityFocus] = AttentionModule.PROB_TRESHOLD;
        else AttentionModule.probability[this.entityFocus] *= 1.2; // non-linear increase

        // normalize probabilities
        let sum = 0;
        for (var key in AttentionModule.probability) sum += AttentionModule.probability[key];
        for (var key in AttentionModule.probability) AttentionModule.probability[key] /= sum;

        // set proactivity for the device that has the focus of attention
        AttentionModule.proactivity[this.entityFocus] = AttentionModule.PROACTIVITY_DEFAULT;

        // send message to Engine
        this.producer.onMessageWrite();
        this.onMessageWrite();
        this.producer.sendMessage();
    }

    /// Sends an attention message to the Engine
    onMessageWrite() {
        this.message.header.eventName = "onAttentionFocus";
        this.message.body.entityFocus = this.entityFocus;
        this.message.body.probability = AttentionModule.probability[this.entityFocus];
        this.message.body.proactivity = AttentionModule.proactivity[this.entityFocus];
    }
    start() {
        this.consumer.start();
        this.producer.start();
        this.producer.pause();
    }
    stop() {
        this.consumer.stop();
        this.producer.stop();
    }
}

AttentionModule.probability = {}; // probability of attention, per device
AttentionModule.PROB_TRESHOLD = 1;
AttentionModule.proactivity = {}; // proactivity, per device
AttentionModule.PROACTIVITY_DEFAULT = 10; // default proactivity, in seconds
class Vuzix {
    constructor(euphoriaURL, deviceName, deviceIP) {
        this.producer = new EuphoriaProducer(euphoriaURL, "Vuzix", deviceName, deviceIP, this.onData);
        this.message = this.producer.message;
        this.onData = this.onData.bind(this);
    }
    onData() {
        this.message.body.notificationContent = "";
        this.message.header.eventName = "onNotification";
    }
    start() {
        this.producer.start();
    }
    stop() {
        this.producer.stop();
    }
    pause() {
        this.producer.pause();
    }
    resume() {
        this.producer.resume();
    }

}
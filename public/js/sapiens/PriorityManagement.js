class PriorityManagement {
    constructor(euphoriaOutputURL, euphoriaInputURL, IP, events) {
        this.consumer = new EuphoriaConsumer(euphoriaOutputURL, events, this.onMessageRead.bind(this));
        this.producer = new EuphoriaProducer(euphoriaInputURL, "PriorityManagement", "PriorityManagement", IP, this.onMessageWrite, true);
        this.message = this.producer.message;
        this.onMessageWrite = this.onMessageWrite.bind(this);
    }
    onMessageRead(message) {
        this.producer.onMessageWrite();
        this.onMessageWrite();
        this.producer.sendMessage();
    }
    onMessageWrite() {
        this.message.header.eventName = "onPriorityChange";
        this.message.body = PriorityManagement.PriorityLevels;
    }
    start() {
        this.consumer.start();
        this.producer.start();
        this.producer.pause();
    }
    stop() {
        this.consumer.stop();
        this.producer.stop();
    }
}
PriorityManagement.PriorityLevels = { "WallDisplay": "medium", "Smartphone": "none", "Tablet": "medium", "SoundSystem": "low" };